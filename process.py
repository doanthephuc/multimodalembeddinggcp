from dotenv import load_dotenv
from vertexai.vision_models import MultiModalEmbeddingModel
from vertexai.preview.vision_models import Image
import os
from elasticsearch import Elasticsearch

load_dotenv()

# connect to elastic search cluster
# create index if it's not existing
es = Elasticsearch([os.getenv("ELASTIC_SEARCH_DOMAIN")])
index_name = "product_vectors"
if not es.indices.exists(index=index_name):
    es.indices.create(
        index=index_name,
        body={
            "mappings": {
                "properties": {
                    "vector_of_image": {
                        "type": "dense_vector",
                        "dims": 1408,  # specify the dimensionality of your vectors
                    },
                    "image_name": {"type": "text"},
                }
            }
        },
    )


def embedding_images():
    # get the list of images
    folder_path = "product_images"
    files = os.listdir(folder_path)
    image_files = [file for file in files]

    model = MultiModalEmbeddingModel.from_pretrained("multimodalembedding@001")
    for file in image_files:
        print("embedding vector for " + file)

        image = Image.load_from_file("product_images/" + file)
        # 1408 is the default dimensions quantity
        embeddings = model.get_embeddings(image=image, dimension=1408)
        image_embedding = embeddings.image_embedding

        # index the vector into elastic search index
        # to simplify the task, the 'id' field will not be provided
        document = {"vector_of_image": image_embedding, "image_name": file}
        es.index(index=index_name, body=document)


def search_related_images(content):
    # embdding the text content
    model = MultiModalEmbeddingModel.from_pretrained("multimodalembedding@001")
    embeddings = model.get_embeddings(contextual_text=content, dimension=1408)
    text_embedding = embeddings.text_embedding

    # use cosine similarity to compare vectors
    search_query = {
        "query": {
            "script_score": {
                "query": {"match_all": {}},
                "script": {
                    "source": "cosineSimilarity(params.queryVector, 'vector_of_image') + 1.0",
                    "params": {"queryVector": text_embedding},
                },
            }
        }
    }

    results = es.search(index=index_name, body=search_query)

    for hit in results["hits"]["hits"]:
        print(f"Score: {hit['_score']} - Image: {hit['_source']['image_name']}")


# embedding_images()

search_related_images("black shirt")
