# MultimodalEmbeddingGCP



## Requirements

- Python >= 3.12.1
- Elasticsearch >= 7.17.17

## Installation
```
cd multimodalembeddinggcp
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
python process.py
```

## Visulization

![](visualization.drawio.png)

## Some words on this repository
- This is a very simple Python code applying Multimodal Embedding of GCP, you can follow the document of it here: [Get multimodal embeddings](https://cloud.google.com/vertex-ai/docs/generative-ai/embeddings/get-multimodal-embeddings).
- [Elasticsearch](https://www.elastic.co/) is not only the vector database that you can use, Cassandra, ChromaDB,... are all fine. Also, this code is using Cosine Similarity method to find vectors, you can also use Euclidean Distance, Jaccard Similarity, Cosine Similarity with L1 Normalization, or more complicated methods..
- To simplify the Elasticsearch installation, I suggest using the [Elasticesearch docker image](https://hub.docker.com/_/elasticsearch).
- Some vector similarity metrics that maybe you can have a look at: [How vector similarity search works](https://labelbox.com/blog/how-vector-similarity-search-works/).
- The basic code here will use default 1408 dimensions vector, you can decrese the number of it.
- Also, don't forget to create a GCP account, then create a service account to claim a credentials.json file, and enable Vertex AI API. Here are some tips on it: [Create access credentials](https://developers.google.com/workspace/guides/create-credentials).
- The `product_images.zip` contains some images of televisions, remotes, sofas, you can try another image dataset for some particular purpose.
